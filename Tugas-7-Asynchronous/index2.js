// Jawaban soal 2

const readBooks = require('./callback.js');
var readBooksPromise = require('./promise.js')

var books = [
    {name: 'LOTR', timeSpent: 3000},
    {name: 'Fidas', timeSpent: 2000},
    {name: 'Kalkulus', timeSpent: 4000}
]

let time = 10000;
let i = 0;

readBooksPromise(time, books[i])
    .then(sisaWaktu => {
        i++;
        readBooksPromise(sisaWaktu, books[i])
            .then(sisaWaktu => {
                i++;
                readBooksPromise(sisaWaktu, books[i])
            })
    })
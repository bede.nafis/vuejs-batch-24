//Soal 1.
/*
buatlah fungsi menggunakan arrow function luas dan keliling
persegi panjang dengan arrow function lalu gunakan let atau const
*/
//Jawaban
const luas_keliling = (panjang, lebar) => {
  console.log("Luas persegi panjang = " + String(panjang*lebar));
  console.log("keliling persegi panjang = " + String(2*(panjang+lebar)));
}
luas_keliling(3,2);

/*
Soal 2
Ubahlah code di bawah ke dalam arrow function dan object literal es6 yang lebih sederhana

const newFunction = function literal(firstName, lastName){
  return {
    firstName: firstName,
    lastName: lastName,
    fullName: function(){
      console.log(firstName + " " + lastName)
    }
  }
}
*/
//Jawaban

const newFunction = (firstName, lastName) => {
  return {
    firstName: firstName,
    lastName: lastName,
    fullName: function(){
      console.log(`${firstName} ${lastName}`)
    }
  }
}
newFunction("William", "Imoh").fullName()

/*
Soal 3
Diberikan sebuah objek sebagai berikut:*/

const newObject = {
  firstName: "Muhammad",
  lastName: "Iqbal Mubarok",
  address: "Jalan Ranamanyar",
  hobby: "playing football",
}
//Gunakan metode destructuring dalam ES6 untuk mendapatkan semua nilai dalam object dengan lebih singkat (1 line saja)

//Jawaban
const {firstName, lastName, address, hobby} = newObject
console.log(firstName, lastName, address, hobby)

/*
Soal 4
Kombinasikan dua array berikut menggunakan array spreading ES6
*/
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
//Jawaban
const combined = [...west, ...east];
console.log(combined);

/*
Soal 5
sederhanakan string berikut agar menjadi lebih sederhana menggunakan
template literals ES6:
*/

const planet = "earth"
const view = "glass"

//var before = 'Lorem ' + view + 'dolor sit amet, ' + 'consectetur adipiscing elit,' + planet

//Jawaban
var before = `Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet}`;
console.log(before);

// Soal 1
var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];

//Jawaban soal 1
daftarHewan.sort()
for (var i=0; i < daftarHewan.length; i++){
  console.log(daftarHewan[i]);
}




//Soal 2
var data = {name : "John" , age : 30 , address : "Jalan Pelesiran" , hobby : "Gaming" }

var perkenalan = introduce(data);
console.log(perkenalan);

//Jawaban Soal 2
function introduce(hasil){
  return "Nama saya " + hasil.name + ", umur saya " + hasil.age + " tahun, alamat saya di " + hasil.address + " , dan saya punya hobby yaitu " + hasil.hobby
}



//Soal 3. Menghitung huruf vokal

//Jawaban
function hitung_huruf_vokal(kata){
  var kata1 = kata;
  var kata2 = kata1.split("");
  var jumlah = 0;

  for (var i=0; i < kata2.length; i++){
    if(kata2[i] == "a" || kata2[i] == "i" || kata2[i] == "u" || kata2[i] == "e" || kata2[i] == "o"){
      jumlah += 1;
    }
    else if(kata2[i] == "A" || kata2[i] == "I" || kata2[i] == "U" || kata2[i] == "E" || kata2[i] == "O"){
      jumlah += 1;
    }
  }
  return jumlah;
}
var hitung_1 = hitung_huruf_vokal("Muhammad")
var hitung_2 = hitung_huruf_vokal("Iqbal")
console.log(hitung_1 , hitung_2);



//Soal 4
function hitung(angka){
  return 2*angka - 2;
}
// Jawaban 4
console.log(hitung(0));
console.log(hitung(1));
console.log(hitung(2));
console.log(hitung(3));
console.log(hitung(5));

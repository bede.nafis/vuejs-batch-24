// Soal 1. Cetak kata-kata "Saya senang belajar JAVASCRIPT"
var pertama = "saya sangat senang hari ini";
var kedua = "belajar javascript itu keren";

// Jawaban
var ketiga = pertama.substring(0,5);
var keempat = pertama.substring(12,19);
var kelima = kedua.substring(0,8);
var keenam = kedua.substring(8,18).toUpperCase();

console.log(ketiga + keempat + kelima + keenam);

//Soal 2. Mengubah variabel kedalam integer, lalu menjumlahkan semua agar hasilnya 24
var kataPertama = "10";
var kataKedua = "2";
var kataKetiga = "4";
var kataKeempat = "6";

//Jawaban
console.log(Number(kataPertama) + (Number(kataKedua)*Number(kataKetiga)) + Number(kataKeempat));



//Soal 3.
var kalimat = 'wah javascript itu keren sekali';

//Jawaban
var kataPertama = kalimat.substring(0, 3);
var kataKedua = kalimat.substring(4, 14); // do your own!
var kataKetiga = kalimat.substring(15, 18); // do your own!
var kataKeempat = kalimat.substring(19, 24); // do your own!
var kataKelima = kalimat.substring(25, 31); // do your own!

//Jawaban
console.log('Kata Pertama: ' + kataPertama);
console.log('Kata Kedua: ' + kataKedua);
console.log('Kata Ketiga: ' + kataKetiga);
console.log('Kata Keempat: ' + kataKeempat);
console.log('Kata Kelima: ' + kataKelima);
